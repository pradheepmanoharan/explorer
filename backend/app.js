const express = require("express");
const app = express();
const mongodb = require("mongodb");
const mongoose = require("mongoose");
const bodyparser = require("body-parser");
const jwt = require("jsonwebtoken");

const News = require("./models/news");
const User = require("./models/user");
const checkAuth = require("./middleware/check-authorization");

app.use(bodyparser.json());
app.use(bodyparser.urlencoded({ extended: false }));
app.use((req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Headers",
    "Origin,X-Requested-With,Content-Type,Accept, Authorization"
  );
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET,POST,PATCH,DELETE,PUT,OPTIONS"
  );
  next();
});

mongoose.connect(
  "mongodb+srv://pradheep:pradheep@explorer.heueb.mongodb.net/Explorer?retryWrites=true&w=majority",
  (error, client) => {
    if (client) {
      console.log("Database connection established successfully");
    } else {
      console.log(error);
    }
  }
);

app.post("/signup", (req, res) => {
  const user = new User({ email: req.body.email, password: req.body.password });
  user
    .save()
    .then((result) => {
      if (result) {
        const token = jwt.sign(
          { email: result["email"], userId: result["_id"] },
          "secret_string",
          { expiresIn: "1h" }
        );

        res
          .status(201)
          .json({ message: "User Created", result: token, response: result });
      }
    })
    .catch((error) => {
      console.log(error);
      res.status(500).json({ error: "Email already exists" });
    });
});

app.post("/login", (req, res) => {
  User.find({ email: req.body.email, password: req.body.password })
    .then((result) => {
      if (result) {
        const token = jwt.sign(
          { email: result[0].email, userId: result[0]._id },
          "secret_string",
          { expiresIn: "1h" }
        );
        res
          .status(201)
          .json({ message: "Login Success", result: token, response: result });
      }
    })
    .catch((error) => {
      res.status(500).json({ error: "Authentication failed" });
    });
});

app.post("/postnews", checkAuth, (req, res) => {
  if (req.body) {
    const newsObj = {
      title: req.body.title,
      publishedAt: req.body.publishedAt,
      source: req.body.source,
      url: req.body.url,
      creator: req.UserData.userId,
    };
    console.log(newsObj);
    const news = new News(newsObj);
    news
      .save()
      .then((result) => {
        res.send("Data added successfully");
      })
      .catch((error) => {
        res.send(error);
      });
  } else {
    console.log("Data not found to post to database");
  }
});
let creatorId;
app.get("/getnews", checkAuth, (req, res) => {
  this.creatorId = req.UserData.userId;
  News.find({ creator: req.UserData.userId })
    .then((result) => {
      res.send(result);
    })
    .catch((error) => {
      res.send("Unable to fetch data");
    });
});

app.delete("/deletenews", checkAuth, (req, res) => {
  News.deleteOne({
    _id: new mongodb.ObjectID(req.body._id),
    creator: req.UserData.userId,
  })
    .then((result) => {
      res.send("Data deleted successfully");
    })
    .catch((error) => {
      res.send("Unable to fetch data");
    });
});

app.delete("/deleteallnews", (req, res) => {
  News.deleteMany({ creator: this.creatorId })
    .then((result) => {
      res.send("All Data deleted successfully");
    })
    .catch((error) => {
      res.send("Unable to fetch data");
    });
});

module.exports = app;
