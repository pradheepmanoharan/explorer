const mongoose = require("mongoose");
const News = mongoose.model("News", {
  title: { type: String },
  publishedAt: { type: String },
  source: { type: String },
  url: { type: String },
  creator: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User",
    required: true,
  },
});

module.exports = News;
