import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Router } from "@angular/router";
import { tap } from "rxjs/operators";
@Injectable({
  providedIn: "root",
})
export class AuthenticationService {
  constructor(private http: HttpClient, private router: Router) {}
  signup(email: string, password: string) {
    return this.http
      .post<{
        message: string;
        result: string;
        response: object;
      }>("http://localhost:3000/signup", {
        email: email,
        password: password,
      })
      .pipe(
        tap((response) => {
          localStorage.setItem("email", response.response["email"]);
        })
      );
  }

  login(email: string, password: string) {
    return this.http
      .post<{ message: string; result: string; response: object }>(
        "http://localhost:3000/login",
        {
          email: email,
          password: password,
        }
      )
      .pipe(
        tap((response) => {
          localStorage.setItem("email", response.response[0].email);
        })
      );
  }

  logout() {
    window.localStorage.clear();
    location.reload();
    this.router.navigate(["/authentication"]);
  }
}
