import { Injectable } from "@angular/core";
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  Router,
} from "@angular/router";
import { Observable } from "rxjs";
import { AuthenticationService } from "./authentication.service";
@Injectable()
export class AuthGuard implements CanActivate {
  isAuth;
  constructor(
    private authService: AuthenticationService,
    private router: Router
  ) {}
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> | Promise<boolean> | boolean {
    this.isAuth = true;
    if (!localStorage.getItem("token")) {
      this.router.navigate(["/authentication"]);
      this.isAuth = false;
    }

    return this.isAuth;
  }
}
