import { Injectable, NgModule } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { BehaviorSubject } from "rxjs";
import { DatePipe } from "@angular/common";
import { news } from "../models/news.model";
import { AuthenticationService } from "./authentication.service";

@Injectable({
  providedIn: "root",
})
export class NewsService {
  isLoading = false;
  isLoadingSubject = new BehaviorSubject<any>(null);
  postNewsArr: news[] = [];
  getNewsArr: news[] = [];
  postNewsSubject = new BehaviorSubject<news[]>(null);
  getNewsSubject = new BehaviorSubject<news[]>(null);
  allDataDeleted = new BehaviorSubject<any>(null);
  token;
  headers = new HttpHeaders();
  constructor(
    private http: HttpClient,
    private datepipe: DatePipe,
    private authService: AuthenticationService
  ) {
    this.token = localStorage.getItem("token");
  }

  fetchNews(keyword: string) {
    this.isLoading = true;
    this.isLoadingSubject.next(this.isLoading);
    return this.http
      .get<news>(
        `https://newsapi.org/v2/everything?q="${keyword}"&searchin=title&from=2022-03-18&sortBy=publishedAt&apiKey=895a435851474216bb06a35200572dc9&pageSize=100&page=1&language=en&sortBy=popularity`
      )
      .subscribe(
        (resData) => {
          for (let i = 0; i < resData["articles"].length; i++) {
            const news: news = {
              title: resData["articles"][i].title,
              publishedAt: this.datepipe.transform(
                resData["articles"][i].publishedAt,
                "yyyy-MM-dd"
              ),
              source: resData["articles"][i].source.name,
              url: resData["articles"][i].url,
            };

            this.postNewsArr.push(news);
          }

          this.postNewsSubject.next(this.postNewsArr);
          this.isLoading = false;
          this.isLoadingSubject.next(this.isLoading);
          this.postNewsArr = [];
        },
        (errorResponse) => {
          let errorMessage = "An unknown error occured";
          if (!errorResponse.error || !errorResponse.error.status) {
            console.log(errorMessage);
          } else {
            switch (errorResponse.error.code) {
              case "apiKeyInvalid":
                errorMessage = errorResponse.error.message;
                break;
              case "apiKeyDisabled":
                errorMessage = errorResponse.error.message;
                break;
              case "unexpectedError":
                errorMessage = errorResponse.error.message;
                break;
              case "rateLimited":
                errorMessage = errorResponse.error.message;
                break;
              case "parameterInvalid":
                errorMessage = errorResponse.error.message;
                break;
            }
            console.log(errorMessage);
          }
        }
      );
  }

  postData(news: news) {
    this.http
      .post("http://localhost:3000/postnews", news, { responseType: "text" })
      .subscribe((resData) => {
        console.log(resData);
      });
  }

  getData() {
    this.http
      .get("http://localhost:3000/getnews", { responseType: "text" })
      .subscribe((resData) => {
        this.getNewsArr = JSON.parse(resData);
        this.getNewsSubject.next(this.getNewsArr);
        this.getNewsArr = [];
      });
  }

  deleteData(news: news) {
    return this.http.request("delete", "http://localhost:3000/deletenews", {
      body: news,
      responseType: "text",
    });
  }

  deleteAllData() {
    this.http
      .delete("http://localhost:3000/deleteallnews", { responseType: "text" })
      .subscribe((response) => {
        return this.allDataDeleted.next("delete all data clicked");
      });
  }

  get sendIsLoading() {
    return this.isLoadingSubject.asObservable();
  }
}
