import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { DatePipe } from "@angular/common";
import { AppRoutingModule } from "./app-routing.module";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { MaterialModule } from "./modules/material.module";
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { FormsModule } from "@angular/forms";
import { ReactiveFormsModule } from "@angular/forms";

import { AppComponent } from "./app.component";
import { AuthenticationComponent } from "./components/authentication/authentication.component";
import { NewsComponent } from "./components/news/news.component";
import { HeaderComponent } from "./components/news/header/header.component";
import { SearchformComponent } from "./components/news/search-form/search-form.component";
import { ApiResultsComponent } from "./components/news/api-results/api-results.component";
import { DatabaseResultsComponent } from "./components/news/database-results/database-results.component";
import { AuthenticationHeaderComponent } from "./components/authentication/authentication-header/authentication-header.component";
import { AuthenticationFormComponent } from "./components/authentication/authentication-form/authentication-form.component";
import { AuthInterceptor } from "./services/auth-interceptor";

@NgModule({
  declarations: [
    AppComponent,
    AuthenticationComponent,
    NewsComponent,
    HeaderComponent,
    SearchformComponent,
    ApiResultsComponent,
    DatabaseResultsComponent,
    AuthenticationHeaderComponent,
    AuthenticationFormComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
  ],
  // providers: [DatePipe],
  providers: [{ provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },DatePipe],
  bootstrap: [AppComponent],
})
export class AppModule {}
