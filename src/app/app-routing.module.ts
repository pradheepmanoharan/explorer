import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { AuthenticationComponent } from "./components/authentication/authentication.component";
import { NewsComponent } from "./components/news/news.component";
import { AuthGuard } from "./services/auth-guard";

const routes: Routes = [
  {
    path: "authentication",
    component: AuthenticationComponent,
  },

  {
    path: "news",
    component: NewsComponent,
    canActivate: [AuthGuard],
  },
  { path: "", redirectTo: "/authentication", pathMatch: "full" },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [AuthGuard],
})
export class AppRoutingModule {}
