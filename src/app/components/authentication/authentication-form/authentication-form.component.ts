import { Component, OnInit } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { AuthenticationService } from "src/app/services/authentication.service";
import { ValidatorFn } from "@angular/forms";
import { ValidationErrors } from "@angular/forms";
import { Router } from "@angular/router";
import { Subscription, Observable } from "rxjs";

@Component({
  selector: "app-authentication-form",
  templateUrl: "./authentication-form.component.html",
  styleUrls: ["./authentication-form.component.css"],
})
export class AuthenticationFormComponent implements OnInit {
  authObservable: Observable<{ message: string; result: string }>;
  privateAuthListenerSubs: Subscription;
  isAuthenticated;
  credentialsForm: FormGroup;
  inLoginMode = true;
  errors = null;
  isLoading = false;
  displayError = false;
  constructor(
    private authenticationService: AuthenticationService,
    private router: Router
  ) {}
  ngOnInit() {
    this.credentialsForm = new FormGroup(
      {
        email: new FormControl("", [Validators.required, Validators.email]),
        password: new FormControl("", [
          Validators.required,
          Validators.minLength(6),
        ]),
        confirmPassword: new FormControl("", [Validators.required]),
      },

      { validators: passwordMatchValidator }
    );
  }

  onInput() {
    this.displayError = false;
  }

  switchMode() {
    this.displayError = false;
    this.isLoading = true;
    setTimeout(() => {
      this.isLoading = false;
      this.inLoginMode = !this.inLoginMode;
      this.credentialsForm.reset();
    }, 800);
  }

  onSubmit() {
    this.isLoading = true;

    if (this.inLoginMode) {
      this.authObservable = this.authenticationService.login(
        this.credentialsForm.value.email,
        this.credentialsForm.value.password
      );
    } else {
      this.authObservable = this.authenticationService.signup(
        this.credentialsForm.value.email,
        this.credentialsForm.value.password
      );
    }

    this.authObservable.subscribe(
      (response) => {
        if (response.result) {
          this.isLoading = false;
          const token = response.result;
          localStorage.setItem("token", token);
          this.router.navigate(["/news"]);
        }
      },
      (errorResponse) => {
        console.log(errorResponse.error.error);
        if (errorResponse["status"] === 500) {
          this.isLoading = false;
          this.errors = errorResponse.error.error;
          this.displayError = true;
        }
      }
    );

    this.credentialsForm.reset();
  }
}

export const passwordMatchValidator: ValidatorFn = (
  formGroup: FormGroup
): ValidationErrors | null => {
  return formGroup.get("password").value ===
    formGroup.get("confirmPassword").value
    ? null
    : { passwordMismatch: true };
};
