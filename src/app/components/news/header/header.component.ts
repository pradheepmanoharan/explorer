import { Component, OnInit } from "@angular/core";
import { AuthenticationService } from "src/app/services/authentication.service";
import { Subscription } from "rxjs";

import { NewsService } from "src/app/services/news.service";

@Component({
  selector: "app-header",
  templateUrl: "./header.component.html",
  styleUrls: ["./header.component.css"],
})
export class HeaderComponent implements OnInit {
  email: string;
  constructor(
    private newsService: NewsService,
    private authenticationService: AuthenticationService
  ) {}

  ngOnInit() {
    this.email = localStorage.getItem("email");
  }

  getData() {
    this.newsService.getData();
  }

  onLogout() {
    this.authenticationService.logout();
  }
}
