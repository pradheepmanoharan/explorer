import { Component, OnInit } from "@angular/core";
import { news } from "src/app/models/news.model";
import { DatePipe } from "@angular/common";
import { FormGroup, FormControl } from "@angular/forms";
import { BehaviorSubject } from "rxjs";
import { NewsService } from "src/app/services/news.service";
@Component({
  selector: "app-search-form",
  templateUrl: "./search-form.component.html",
  styleUrls: ["./search-form.component.css"],
})
export class SearchformComponent implements OnInit {
  searchForm: FormGroup;

  newsArr: news[] = [];
  constructor(private newsservice: NewsService, private datepipe: DatePipe) {}

  ngOnInit() {
    this.searchForm = new FormGroup({
      keyword: new FormControl(null),
    });
  }

  onSubmit() {
    console.log(this.searchForm.value.keyword);

    this.newsservice.fetchNews(this.searchForm.value.keyword);

    this.searchForm.reset();
  }
}
