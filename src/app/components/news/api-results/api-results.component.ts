import { Component, OnInit, ViewChild, OnDestroy } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { MatPaginator, MatTableDataSource } from "@angular/material";
import { Subscription } from "rxjs";
import { news } from "src/app/models/news.model";
import { NewsService } from "src/app/services/news.service";

@Component({
  selector: "app-api-results",
  templateUrl: "./api-results.component.html",
  styleUrls: ["./api-results.component.css"],
})
export class ApiResultsComponent implements OnInit {
  @ViewChild("MatPaginator") paginator: MatPaginator;
  newsArray: news[] = [];
  ishidden = true;
  isLoading;

  //MatTABLE
  displayedColumns: string[] = ["title", "publishedAt", "url", "save"];
  dataSource;

  private newssub: Subscription;

  constructor(private http: HttpClient, private newsservice: NewsService) {}

  ngOnInit() {
    this.newssub = this.newsservice.postNewsSubject.subscribe((data) => {
      if (data) {
        this.newsArray = data;
        const dataSource = new MatTableDataSource(this.newsArray);
        this.dataSource = dataSource;
        dataSource.paginator = this.paginator;
        this.ishidden = false;
      } else {
        console.log("no data");
      }
    });

    this.newsservice.isLoadingSubject.subscribe((Response) => {
      console.log(Response);
      this.isLoading = Response;
    });
  }

  postData(row: any) {
    this.newsservice.postData(row);
  }

  hideTable() {
    this.ishidden = true;
    this.newsArray.length = 0;
  }
  ngOnDestroy() {
    this.newssub.unsubscribe();
  }
}
