import { Component, OnInit, ViewChild } from "@angular/core";
import { HostListener } from "@angular/core";
import { AuthenticationService } from "src/app/services/authentication.service";
import { news } from "src/app/models/news.model";
@Component({
  selector: "app-data",
  templateUrl: "./news.component.html",
  styleUrls: ["./news.component.css"],
})
export class NewsComponent implements OnInit {
  constructor(private authenticationService: AuthenticationService) {}
  ngOnInit() {}

  @HostListener("window:popstate", ["$event"])
  onPopState(event) {
    this.authenticationService.logout();
  }
}
