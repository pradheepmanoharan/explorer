import { Component, OnInit, ViewChild, OnDestroy } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { MatPaginator, MatTableDataSource } from "@angular/material";
import { Subscription } from "rxjs";
import { news } from "src/app/models/news.model";
import { NewsService } from "src/app/services/news.service";

@Component({
  selector: "app-database-results",
  templateUrl: "./database-results.component.html",
  styleUrls: ["./database-results.component.css"],
})
export class DatabaseResultsComponent implements OnInit {
  @ViewChild("MatPaginator") paginator: MatPaginator;
  newsArray: news[] = [];
  ishidden = true;

  //MatTABLE
  displayedColumns: string[] = ["title", "publishedAt", "url", "delete"];
  dataSource;

  private newssub: Subscription;
  constructor(private http: HttpClient, private newsservice: NewsService) {}

  ngOnInit() {
    this.newssub = this.newsservice.getNewsSubject.subscribe((data) => {
      if (data) {
        this.newsArray = data;
        const dataSource = new MatTableDataSource(this.newsArray);
        this.dataSource = dataSource;
        dataSource.paginator = this.paginator;
        this.ishidden = false;
        console.log(data);
      } else {
        console.log("no data");
      }
    });
  }

  deleteData(row: news) {
    this.newsservice.deleteData(row).subscribe((resData) => {
      console.log(resData);
      if (resData) {
        var index = this.newsArray.indexOf(row);
        if (index > -1) {
          this.newsArray.splice(index, 1);
          this.ngOnInit();
        } else {
          console.log("no");
        }
      }
    });
  }

  deleteallData() {
    this.newsservice.deleteAllData();
    this.newsservice.allDataDeleted.subscribe((response) => {
      if (response !== null) {
        this.newsArray.length = 0;
      }
      this.ngOnInit();
    });
  }
  hideTable() {
    this.ishidden = true;
    this.newsArray.length = 0;
  }
  ngOnDestroy() {
    this.newssub.unsubscribe();
  }
}
