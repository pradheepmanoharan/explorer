export interface news {
  _id?: string;
  title: string;
  publishedAt: string;
  source: string;
  url: string;
}
