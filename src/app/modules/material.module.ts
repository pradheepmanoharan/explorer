import { NgModule } from "@angular/core";
import { MatInputModule } from "@angular/material/input";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatButtonModule } from "@angular/material/button";
import { MatCardModule } from "@angular/material";
import { MatProgressSpinnerModule } from "@angular/material";
import { MatIconModule } from "@angular/material";
import { MatTableModule } from "@angular/material/table";
import { MatPaginatorModule } from "@angular/material/paginator";
import { MatRippleModule } from "@angular/material";
import { MatToolbarModule } from "@angular/material/toolbar";
const module = [
  MatFormFieldModule,
  MatInputModule,
  MatButtonModule,
  MatCardModule,
  MatProgressSpinnerModule,
  MatIconModule,
  MatTableModule,
  MatPaginatorModule,
  MatRippleModule,
  MatToolbarModule,
];

@NgModule({
  declarations: [],
  imports: module,
  exports: module,
})
export class MaterialModule {}
